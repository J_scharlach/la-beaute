/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it
import "./src/styles/style.scss"
import "bootstrap/dist/css/bootstrap.min.css";
import "firebase/auth"
import "firebase/firestore"
import React from 'react'
import AuthProvider from "./src/context/auth";

export const wrapRootElement = ({ element }) =>{
    return(
        <AuthProvider>{element}</AuthProvider>
    )
}

export const onServiceWorkerUpdateReady = () => {
    const answer = window.confirm(
      `This application has been updated. ` +
        `Reload to display the latest version?`
    )
    if (answer === true) {
      window.location.reload()
    }
  }

export const registerServiceWorker = () => true