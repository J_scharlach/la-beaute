require('dotenv').config()

module.exports = {
  pathPrefix: "/gatsby-react-bootstrap-starter",
  siteMetadata: {
    title: `La beaute infinie`,
    description: `A beauty salon`,
    author: `Joel Scharlach`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images/`,
      },
    },
    `gatsby-plugin-sass`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `La beaute infinie`,
        short_name: `La beaute infinie`,
        start_url: `/`,
        background_color: `#20232a`,
        theme_color: `#20232a`,
        display: `standalone`,
        icon: "src/favicon.png",
      },
    },
    {
      resolve: "gatsby-plugin-firebase",
      options: {
        credentials: {
      apiKey: "AIzaSyCweOmkZTtJJyAhCPbqYMC3reKKb4V89PA",
      authDomain: "labeauteinfinie-6507d.firebaseapp.com",
      projectId: "labeauteinfinie-6507d",
      storageBucket: "labeauteinfinie-6507d.appspot.com",
      messagingSenderId: "1016851416008",
      appId: "1:1016851416008:web:3923a7715d999e9c8cf6fd",
      measurementId: "G-1HPT92XVRB"
        }
      }
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    `gatsby-plugin-offline`,
  ],
}
