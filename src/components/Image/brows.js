import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

import styles from './fourpics.module.css'
import Img from 'gatsby-image';

export default function logonav () {
  return (
    <StaticQuery
      query={graphql`
      query BrowsImg {
        file(relativePath: {eq: "br.png"}) {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      `}
      render={data => (
        <div >
          <Img className={styles.dataimg} fluid={data.file.childImageSharp.fluid} />
        </div>
      )}
    />
  )
}