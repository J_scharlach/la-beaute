import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

import styles from './heist.module.css'
import Img from 'gatsby-image';

export default function logonav () {
  return (
    <StaticQuery
      query={graphql`
      query HesitImg {
        file(relativePath: {eq: "heist.png"}) {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      `}
      render={data => (
        <div >
          <Img className={styles.heist} fluid={data.file.childImageSharp.fluid} />
        </div>
      )}
    />
  )
}