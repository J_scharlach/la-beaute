import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

import styles from './lipstick.module.css'
import Img from 'gatsby-image';

export default function logonav () {
  return (
    <StaticQuery
      query={graphql`
      query Lip {
        file(relativePath: {eq: "lip.png"}) {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      `}
      render={data => (
        <div >
          <Img className={styles.lip} fluid={data.file.childImageSharp.fluid} />
        </div>
      )}
    />
  )
}