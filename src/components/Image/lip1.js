import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

import styles from './lipstick.module.css'
import Img from 'gatsby-image';

export default function logonav () {
  return (
    <StaticQuery
      query={graphql`
      query Lip1 {
        file(relativePath: {eq: "lip22.png"}) {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      `}
      render={data => (
        <div >
          <Img className={styles.lip} fluid={data.file.childImageSharp.fluid} />
        </div>
      )}
    />
  )
}