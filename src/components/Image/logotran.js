import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

import Img from 'gatsby-image';
import styles from './logotran.module.css'

export default function AboutImg () {
  return (
    <StaticQuery
      query={graphql`
      query ImagePic {
        file(relativePath: {eq: "saying.png"}) {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      `}
      render={data => (
        <div>
          <Img className={styles.dataimg} fluid={data.file.childImageSharp.fluid} />
        </div>
      )}
    />
  )
}
