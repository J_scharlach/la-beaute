import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

import styles from './fourpics.module.css'
import Img from 'gatsby-image';

export default function mass () {
  return (
    <StaticQuery
      query={graphql`
      query MassImg {
        file(relativePath: {eq: "mass.jpg"}) {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      `}
      render={data => (
        <div >
          <Img className={styles.dataimg} fluid={data.file.childImageSharp.fluid} />
        </div>
      )}
    />
  )
}