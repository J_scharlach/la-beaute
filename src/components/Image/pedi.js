import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

import styles from './fourpics.module.css'
import Img from 'gatsby-image';

export default function pedi () {
  return (
    <StaticQuery
      query={graphql`
      query PediImg {
        file(relativePath: {eq: "pedi.jpg"}) {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      `}
      render={data => (
        <div >
          <Img className={styles.dataimg} fluid={data.file.childImageSharp.fluid} />
        </div>
      )}
    />
  )
}