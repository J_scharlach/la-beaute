import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

import Img from 'gatsby-image';
import styles from './pricelist.module.css'

export default function AboutImg () {
  return (
    <StaticQuery
      query={graphql`
      query pl1Pic {
        file(relativePath: {eq: "pl1.png"}) {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      `}
      render={data => (
        <div>
          <Img className={styles.dataimg} fluid={data.file.childImageSharp.fluid} />
        </div>
      )}
    />
  )
}
