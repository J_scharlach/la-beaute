import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

import styles from './profileImg.module.css'
import Img from 'gatsby-image';

export default function logonav () {
  return (
    <StaticQuery
      query={graphql`
      query ProfileImg {
        file(relativePath: {eq: "transprofile.png"}) {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      `}
      render={data => (
        <div >
          <Img className={styles.imgprofile} fluid={data.file.childImageSharp.fluid} />
        </div>
      )}
    />
  )
}