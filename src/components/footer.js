import React from "react";
import { Row, Col, Button } from "react-bootstrap"
import { Link } from 'gatsby'
import styles from './footer.module.css'
import LogoNav from '../components/Image/logonav'
import Heist from '../components/Image/heist'
import { FaFacebookSquare, FaInstagramSquare } from 'react-icons/fa';

const Footer = () => {
  return (
    <div className={styles.footerclassic}>
          <Row>
            <Col md={4} lg={5}>
                <div className={styles.logo}>
                    <LogoNav/>
                </div>
                {/* <div className={styles.logo}>     
                    <p className={styles.rights}>BLAH BLAH BLAH</p>
                </div> */}
                <div className={styles.logo1}>
                    <Heist className={styles.heist}/>
                </div>    
            </Col>
            <Col md={4}>
              <h5>Contacts</h5>
              <dl className={styles.contactlist}>
                <dt>Address:</dt>
                <dd>Voetweg 4A, 2220 Heist o / d Berg</dd>
              </dl>
              <dl className={styles.contactlist}>
                <dt>email:</dt>
                <dd><Link to="mailto:#"><span  className={styles.emailinfo}>marine.dinant@gmail.col</span></Link></dd>
              </dl>
              <dl className={styles.contactlist}>
                <dt>phones:</dt>
                <p>0478 91 49 73</p>
              </dl>
            </Col>
            <Col md={4} lg={3}>
              <h5>Links</h5>
              <ul className={styles.navlist}>
                <li><Link to="/about"><span  className={styles.emailinfo}>About</span></Link></li>
                <li><Link to="/apt"><span  className={styles.emailinfo}>Make Appointment</span></Link></li>
                <li><Link to="/services"><span  className={styles.emailinfo}>Treatments</span></Link></li>
                <li><Link to="/product"><span  className={styles.emailinfo}>Products</span></Link></li>
                <li><Link to="/apt"><Button className={styles.btnapt} size="lg">Book an apt</Button></Link></li>
              </ul>
            </Col>
          </Row>
        <div className={styles.container}>
            <Row className={styles.rowsocial}>
                <Col xs={3}>
                    <div className={styles.rowsocial}>
                    <FaFacebookSquare className={styles.iconq} />
                    </div>
                    <div className={styles.col}><Link className={styles.socialinner} to="#"><span className={styles.icon}></span><span>Facebook</span></Link></div>
                </Col> 
                <Col xs={3}>
                    <div className={styles.rowsocial}>
                    <FaInstagramSquare className={styles.iconq} /> 
                    </div>
                    <div className={styles.col}><Link className={styles.socialinner} to="#"><span className={styles.icon}></span><span>instagram</span></Link></div>
                </Col> 
            </Row>
        </div>
      </div>
  )
}

export default Footer