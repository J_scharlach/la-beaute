
import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"

import { Row, Col } from "react-bootstrap"
// import styles from '../styles/index.module.css'
// import Header from "./header"
import Navbars from "./navBar"
import Footer from "./footer.js"

const Layout = ({ children}) => {

  return (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <div fluid className="p-0 main">
          {/* <Row noGutters className="justify-content-center">
            <Col>
              <Header siteTitle={data.site.siteMetadata.title} />
            </Col>
          </Row> */}
          <Navbars />
          <Row noGutters>
            <Col>
              <div>
                <main>{children}</main>
              </div>
            </Col>
          </Row>
        </div>
        <div fluid className="px-0">
          <Row noGutters>
            <Col className="footer-col">
            <Footer />
              <footer>
                <span>
                  © {new Date().getFullYear()}, Built by
                  {` `}
                  <Link href="https://xlixelit.web.app/">XLIX elit</Link>
                </span>
              </footer>
            </Col>
          </Row>
        </div>
      </>
    )}
  />
  )
}

export default Layout
