
import React, { useState, useContext, useEffect } from "react";
import {AuthContext} from "../context/auth"
import { Link, navigate } from 'gatsby'
import LogoNav from '../components/Image/logonav'
import { FaBars, FaUndoAlt, FaUser } from 'react-icons/fa';
import firebase from "gatsby-plugin-firebase"
import { firestore } from "../utils/firebase"



const Navbars = () => {
  
  const { user } = useContext(AuthContext)

  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const handleLogout = async () =>{
    await firebase.auth().signOut()
    navigate("/")
  }

  const [person, setPerson] = useState([])

  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
      var ref = firestore.collection("users")

      ref.where('user_id', '==', firebase.auth().currentUser.uid).get()
          .then(snapshot => {
            snapshot.forEach(doc => {
            var client = doc.data()
            setPerson(client)
            })
          })
      }else{
          console.log('nope')
      }
    })
  }, [])
  
  return (
  <div class="fixednav">
    <div class="header">
      <div class="logo-nav">
        <div class="logo-container">
          <Link to="/">
            <LogoNav class="logo" />
          </Link>
        </div>
        {/* {person ? <p class="signincolorfname">{person.fname}</p> : null} */}
        <ul class={click ? "nav-options active" : "nav-options"}>
          <li>
            <Link to="/about" class="linkname" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}><span class="option">ABOUT</span></Link>
          </li>
          <li>
            <Link to="/apt" class="linkname" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}><span class="option">MAKE APT</span></Link>
          </li>
          <li>
            <Link to="/services" class="linkname" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}><span class="option">TREATMENTS</span></Link>
          </li>
          <li>
            <Link to="/product" class="linkname" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}> <span class="option">PRODUCTS</span></Link>
          </li>
          <li class="option mobile-option">
            <Link to="/signin" class="linkname" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}>LOGIN</Link>
          </li>
          <li class="option mobile-option">
            <Link to="/signup" class="linkname" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}>REGISTER</Link>
          </li>
          <li class="option mobile-option">
            <Link to="/signup" class="linkname" onClick={handleLogout} onKeyDown={closeMobileMenu}>LOGOUT</Link>
          </li>
          <li class="option mobile-option">
            <Link to="/profile" class="linkname" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}>PROFILE</Link>
          </li>
        </ul>
      </div>
      <ul class="signin-up">
        {!user ?
        <>
          <li class="sign-in">
            <Link to="/signin" class="signincolor" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}>LOGIN</Link>
          </li>
          <li class="sign-in">
            <Link to="/signup" class="signincolor" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}>REGISTER</Link>
          </li>
        </> :
        <>
          <li class="sign-in">
            <Link to="/signup" class="signincolor" onClick={handleLogout} onKeyDown={closeMobileMenu}>LOGOUT</Link>
          </li>
          {person ? 
          <li class="sign-infname">
            <p class="signincolorfname">{person.fname}</p>
          </li> : 
          null}
          <li class="sign-in">
            <Link to="/profile" class="signincolor" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}><FaUser class="icon" /></Link>
          </li>
        </>
        }
        {/* <li class="sign-in">
          <Link to="/signin" class="signincolor" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}>LOGIN</Link>
        </li>
        <li class="sign-in">
          <Link to="/signup" class="signincolor" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}>REGISTER</Link>
        </li>
        <li class="sign-in">
          <Link to="/signup" class="signincolor" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}>LOGOUT</Link>
        </li>
        <li class="sign-in">
          <Link to="/profile" class="signincolor" onClick={closeMobileMenu} onKeyDown={closeMobileMenu}><FaUser class="icon" /></Link>
        </li> */}
      </ul>
      <div class="mobile-menu" onClick={handleClick} onKeyDown={handleClick}>
        {click ? (
          <FaUndoAlt class="menu-icon" />
        ) : (
          <FaBars class="menu-icon" />
        )}
      </div>
    </div>
  </div>  
  )
}

export default Navbars;



// import React, { useState, useContext } from "react";
// import {AuthContext} from "../context/auth"
// import { Link, navigate } from 'gatsby'
// import LogoNav from '../components/Image/logonav'
// import { FaBars, FaUndoAlt, FaUser } from 'react-icons/fa';
// import firebase from "gatsby-plugin-firebase"


// const Navbars = () => {
//   const { user } = useContext(AuthContext)

//   const [click, setClick] = useState(false);
//   const handleClick = () => setClick(!click);
//   const closeMobileMenu = () => setClick(false);

//   const handleLogout = async () =>{
//     await firebase.auth().signOut()
//     navigate("/")
//   }