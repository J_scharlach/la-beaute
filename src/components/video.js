import React from 'react';
import { withPrefix } from 'gatsby';

const Video = ({ src }) => {
    return (
        <video
            autoPlay
            muted
            loop
            playsInline
            style={{
                height: 'auto',
                width: '100%',
                zIndex: '-1',
                objectFit: 'fill',
                objectPosition: 'center'
            }}
            src={withPrefix(src)}
        >
            <source src={withPrefix(src)} type="video/mp4" />
            Your device does not support playing 'video/mp4' videos
        </video>
    )
}

export default Video