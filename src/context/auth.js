import React, { createContext, useState, useEffect, useContext } from "react"
import firebase from 'gatsby-plugin-firebase'

export const AuthContext = createContext({})

export function useAuth() {
    return useContext(AuthContext)
  }

const AuthProvider = ({children}) =>{
    const [user, setUser] = useState()

    useEffect(() => {
        firebase.auth().onAuthStateChanged(user => setUser(user))
    }, [])

    return(
        <AuthContext.Provider value={{ user, setUser}}>
            {children}
        </AuthContext.Provider>

    )
}

export default AuthProvider