import React from 'react'
import Layout from "../components/layout"
import SEO from "../components/seo"
import {  Container } from "react-bootstrap"
import styles from './about.module.css'

function about() {
    return (
    <Layout pageInfo={{ pageName: "about" }}>
    <SEO title="about" />  
    <Container className={styles.topconabout}>
        <div>
            <h1 className={styles.h1title}>ALL THINGS MARINE</h1>
        </div>
    </Container>
    </Layout>   
    )
}

export default about

