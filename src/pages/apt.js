import React, { useEffect, useState } from 'react'
import Layout from "../components/layout"
import SEO from "../components/seo"
import {  Row, Col, Form, Button } from "react-bootstrap"
import styles from './apt.module.css'
import { Link } from "gatsby"
import LogoNav from '../components/Image/logonav'
import { useForm } from '@formcarry/react';
import firebase from "gatsby-plugin-firebase"
import { firestore} from "../utils/firebase"



const Apt = () => {

    // const dateref = useRef(null);
    // const messageref = useRef(null);
    // const lnameref = useRef(null);
    // const emailref = useRef(null);
    // const phoneref = useRef(null);
    // const fnameref = useRef(null);
    const [person, setPerson] = useState([])
    const [datedesired, setDate] = useState('')
    const [messagetoyou, setMessage] = useState('')
    const [timedesired, setTime] = useState('')

    useEffect(() => {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
        var ref = firestore.collection("users")
  
        ref.where('user_id', '==', firebase.auth().currentUser.uid).get()
            .then(snapshot => {
              snapshot.forEach(doc => {
              var client = doc.data()
              setPerson(client)
              })
            })
        }else{
            console.log('nope')
        }
      })
    }, [])

      // Call the `useForm` hook in your function component
    const {state, submit} = useForm({
        id: 'OhHFq7S2pFZ'
    });
    
    // Success message
    if (state.submitted) {

        return <div>
                <div>
                Thank you! We received your submission.
                </div>
                <Button>Return to home page</Button>
                </div>;
    }


function subinfo() {
        firestore.collection("users").doc(person.slug).collection("apts").add({
            date: datedesired,
            notes: messagetoyou,
            treatment: person.fname,
            time: timedesired
          })
          .then(() => {
            console.log("Document successfully written!");
        })
        .catch((error) => {
            console.error("Error writing document: ", error);
        });
}

    return (
    <Layout pageInfo={{ pageName: "login" }}>
    <SEO title="login" />  
        <div className={styles.topconsignin}>
        <Form className={styles.conform} onSubmit={submit}>
            <Row>
                <Col xs={11} md={11}>
                <div className={styles.logindiv}>
                    <div  className={styles.logodiv}>
                        <Link to="/">
                            <LogoNav className={styles.logo} />
                        </Link>
                    </div>
                    <div className={styles.title}>Make an appointment</div>
                    <div className={styles.title2}>Please register first or login.  This simplifies appointment making and notes for future visits.  If not, just add details below.</div>
                    <div className={styles.fields}>
                        <div>
                            <label htmlFor="firstname" className={styles.htmltitle}>First name</label>
                            <div className={styles.username}>
                                <input 
                                type="text" 
                                className={styles.userinput} 
                                placeholder={person.fname}
                                id="firstname"
                                name="firstname"
                                value={person.fname}
                                />
                            </div>
                        </div>
                        <div>
                        <label htmlFor="lastname" className={styles.htmltitle}>Last name</label>
                            <div className={styles.username}>
                                <input 
                                type="text" 
                                className={styles.userinput} 
                                placeholder={person.lname}
                                id="lastname"
                                name="lastname"
                                value={person.lname} 
                                // ref = {lnameref} 
                                />
                            </div>
                        </div>
                        <div>
                            <label htmlFor="emailclient" className={styles.htmltitle}>email</label>
                            <div className={styles.username}>
                                <input 
                                type="emailclient" 
                                className={styles.userinput} 
                                placeholder={person.email} 
                                id="emailclient"
                                name="emailclient"
                                value={person.email} 
                                // ref = {emailref} 
                                />
                            </div>
                        </div>
                        <div>
                            <label htmlFor="phoneclient" className={styles.htmltitle}>Phone Number</label>
                            <div className={styles.username}>
                                <input 
                                type="tel" 
                                className={styles.userinput} 
                                placeholder={person.phone}  
                                id="phoneclient"
                                 name="phoneclient"
                                 value={person.phone} 
                                //  ref = {phoneref}  
                                 />
                                </div>
                        </div>
                        <Row>
                            <Col xs={12} md={6}>
                                <div>
                                    <label htmlFor="datedesired" className={styles.htmltitle}>Date</label>
                                    <div className={styles.username}>
                                        <input 
                                        type="date" 
                                        className={styles.userinput} 
                                        // placeholder={person.phone}  
                                        id="datedesired"
                                        name="datedesired"
                                        onChange={event => setDate(event.target.value)} 
                                        />
                                        </div>
                                </div>
                            </Col>
                            <Col xs={12} md={6}>
                                <div>
                                    <label htmlFor="timedesired" className={styles.htmltitle}>Time</label>
                                    <div className={styles.username}>
                                        <input 
                                        type="time" 
                                        className={styles.userinput} 
                                        // placeholder={person.phone}  
                                        id="timedesired"
                                        name="timedesired"
                                        onChange={event => setTime(event.target.value)} />
                                        </div>
                                </div>
                            </Col>    
                        </Row>
                        {/* <div className={styles.dropdowndiv}>
                            <Dropdown>
                                <Dropdown.Toggle className={styles.dropdownbtn} id="dropdown-basic">
                                    TREATMENT
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item><p>TREATMENT 1</p></Dropdown.Item>
                                    <Dropdown.Item><p>TREATMENT 2</p></Dropdown.Item>
                                    <Dropdown.Item><p>TREATMENT 3</p></Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </div> */}
                        <div>
                            <div>
                            <label htmlFor="messagetoyou" className={styles.htmltitle}>Treatment</label>
                            <textarea 
                            name="messagetoyou"
                            id="messagetoyou" 
                            // ref = {messageref} 
                            rows="4" 
                            cols="50" 
                            onChange={event => setMessage(event.target.value)} 
                            placeholder="Treatment wanted.  As well as any other notes you would like to add.  Will confirm with you about the date and time." ></textarea> 
                            </div>
                        </div>
                        <input type="hidden" name="_gotcha"></input>
                    </div>
                    <button className={styles.signinbutton} type="submit" onClick={subinfo}>Submit apt request</button>
                </div>
                </Col>
            </Row>
        </Form>    
        </div>
    </Layout>
    )
}

export default Apt