import React from "react"
import { Row, Col, Button } from "react-bootstrap"
import { Link } from "gatsby"
import Layout from "../components/layout"
import { FaQuestionCircle, FaCalendarAlt } from 'react-icons/fa';
import SEO from "../components/seo"
import Fade from 'react-reveal/Fade';
import Zoom from 'react-reveal/Zoom';
import LogoTran from '../components/Image/logotran'
import ProfileImg from '../components/Image/profileImg'
import LashesImg from '../components/Image/lashes'
import BrowsImg from '../components/Image/brows'
import MakeupImg from '../components/Image/makeup'
import RemoveImg from '../components/Image/remove'
import PediImg from '../components/Image/pedi'
import MassImg from '../components/Image/mass'
import styles from '../styles/index.module.css'
import Video from '../components/video'


const IndexPage = () => {

  return (
  <Layout pageInfo={{ pageName: "index" }}>
    <SEO title="Home" keywords={[`gatsby`, `react`, `bootstrap`]} />
    <div className={styles.contop}>
      <div className={styles.convideo}>
        <Video src='/clock.mp4' />
      </div>
      <div className={styles.consaying}>
        {/* <canvas id="background" className={styles.background}>
        </canvas>  */}
        <div className={styles.saying}>
          <Zoom top duration={4500} delay={1000} distance="30px">
              <LogoTran />
          </Zoom>
        </div> 
      </div> 
      <div className={styles.whoweare}>
        <Row>
          <Col md={6} className={styles.whoweare}>
            <div>
              <ProfileImg />
            </div>
          </Col>
          <Col md={6} className={styles.whoweare2}>
          <p className={styles.salondiscrip}>about</p>
            <h1 className={styles.salondiscrip}>la beaute infinie</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a erat sem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam lacus mi, commodo vitae nibh nec, ornare pulvinar eros. Curabitur in venenatis sem. Ut nec tincidunt ex. Praesent venenatis maximus urna tincidunt imperdiet. Curabitur sed dapibus mauris. Morbi pellentesque gravida sapien eu vehicula. Nulla dictum arcu ut pellentesque faucibus. Vestibulum sed sapien diam. Aenean finibus vestibulum nulla, non eleifend nunc sodales quis.
              Fusce augue sem, consectetur dapibus eros non, auctor iaculis diam. Nulla in pellentesque turpis. Ut egestas orci vel sem rhoncus, non cursus lorem.</p>
          </Col>
        </Row> 
      </div>
      <div>
        <Fade top cascade duration={2000} distance="30px">
          <h1 className={styles.h1title}>Our Services</h1>
          <p>We Love What We Do & We Do It With Love For You</p>
        </Fade> 
        <div>
          <div className={styles.con4pics}>
            <Row className={styles.row4pics}>
              <Col md={4}>
              <Fade left duration={2000}>
                  <div className={styles.box}>
                    <LashesImg />
                      <div className={styles.boxcontent}>
                          <div className={styles.content}>
                              <h3 className={styles.title}>Lashes</h3>
                              <ul>
                              <li>One by One: 2D, 3D, 4D, 5D Volume Lashes</li>
                              <li>Flare Lashes</li>
                              <li>Lashlift</li>
                              <li>Removing make-up</li>
                              <li>Removing lashes</li>
                              <li>Retouch Lashes</li>
                              </ul>
                              <ul className={styles.icon}>
                                <Row className={styles.rowicons}>
                                  <Col>
                                    {/* <FaQuestionCircle className={styles.iconq} /> */}
                                    <Link to="/services"><Button size="lg" className={styles.iconbtn}><FaQuestionCircle className={styles.iconq} /></Button></Link>
                                  </Col>
                                  <Col>
                                    {/* <FaCalendarAlt className={styles.iconq} /> */}
                                    <Link to="/apt"><Button size="lg" className={styles.iconbtn}><FaCalendarAlt className={styles.iconq} /></Button></Link>
                                  </Col>
                                </Row>
                              </ul>
                          </div>
                      </div>
                  </div>
                </Fade>
              </Col>
              <Col md={4}>
              <Fade right duration={2000}>
                  <div className={styles.box}>
                    <BrowsImg />
                      <div className={styles.boxcontent}>
                          <div className={styles.content}>
                              <h3 className={styles.title}>Eyebrows</h3>
                              <ul>
                                <li>Browshaping</li>
                                <li>Shaping + colouring</li>
                              </ul>
                              <ul className={styles.icon}>
                                <Row className={styles.rowicons}>
                                  <Col>
                                    {/* <FaQuestionCircle className={styles.iconq} /> */}
                                    <Link to="/services"><Button size="lg" className={styles.iconbtn}><FaQuestionCircle className={styles.iconq} /></Button></Link>
                                  </Col>
                                  <Col>
                                    {/* <FaCalendarAlt className={styles.iconq} /> */}
                                    <Link to="/apt"><Button size="lg" className={styles.iconbtn}><FaCalendarAlt className={styles.iconq} /></Button></Link>
                                  </Col>
                                </Row>
                              </ul>
                          </div>
                      </div>
                  </div>
                  </Fade>  
                </Col>
            </Row>
            <Row className={styles.row4pics2}>
              <Col md={4}>
              <Fade left duration={2000}>
                  <div className={styles.box}>
                    <MakeupImg />
                      <div className={styles.boxcontent}>
                          <div className={styles.content}>
                              <h3 className={styles.title}>Make Up</h3>
                              <ul>
                                <li>Day and night</li>
                                <li>With Lashes</li>
                                <li>Bridal makeup</li>
                                <li>Extra guests</li>
                              </ul>
                              <ul className={styles.icon}>
                                <Row className={styles.rowicons}>
                                  <Col>
                                    {/* <FaQuestionCircle className={styles.iconq} /> */}
                                    <Link to="/services"><Button size="lg" className={styles.iconbtn}><FaQuestionCircle className={styles.iconq} /></Button></Link>
                                  </Col>
                                  <Col>
                                    {/* <FaCalendarAlt className={styles.iconq} /> */}
                                    <Link to="/apt"><Button size="lg" className={styles.iconbtn}><FaCalendarAlt className={styles.iconq} /></Button></Link>
                                  </Col>
                                </Row>
                              </ul>
                          </div>
                      </div>
                  </div>
                  </Fade>  
              </Col>
              <Col md={4}>
              <Fade right duration={2000}>
                  <div className={styles.box}>
                    <RemoveImg />
                      <div className={styles.boxcontent}>
                          <div className={styles.content}>
                              <h3 className={styles.title}>Hair Removal</h3>
                              <Row>
                                <Col xs={6}>
                                <ul>
                                  <li>Eyebrow</li>
                                  <li>Upperlip</li>
                                  <li>Chin</li>
                                  <li>Lower leg</li>
                                  <li>Knees</li>
                                </ul>
                                </Col>
                                <Col xs={6}>
                                <ul>
                                  <li>Upper leg</li>
                                  <li>Torso</li>
                                  <li>Back</li>
                                  <li>Arms</li>
                                </ul>
                                </Col>
                              </Row> 
                              {/* <ul>
                                <li>Eyebrow</li>
                                <li>Upperlip</li>
                                <li>Chin</li>
                                <li>Lower leg</li>
                                <li>Knees</li>
                                <li>Upper leg</li>
                                <li>Torso</li>
                                <li>Back</li>
                                <li>Arms</li>
                              </ul> */}
                              <ul className={styles.icon}>
                                <Row className={styles.rowicons}>
                                  <Col>
                                    {/* <FaQuestionCircle className={styles.iconq} /> */}
                                    <Link to="/services"><Button size="lg" className={styles.iconbtn}><FaQuestionCircle className={styles.iconq} /></Button></Link>
                                  </Col>
                                  <Col>
                                    {/* <FaCalendarAlt className={styles.iconq} /> */}
                                    <Link to="/apt"><Button size="lg" className={styles.iconbtn}><FaCalendarAlt className={styles.iconq} /></Button></Link>
                                  </Col>
                                </Row>
                              </ul>
                          </div>
                      </div>
                  </div>
                  </Fade>
                </Col>
            </Row>
            <Row className={styles.row4pics2}>
              <Col md={4}>
              <Fade left duration={2000}>
                  <div className={styles.box}>
                    <PediImg />
                      <div className={styles.boxcontent}>
                          <div className={styles.content}>
                              <h3 className={styles.title}>Pedicure</h3>
                              <ul>
                                <li>Basic pedicure</li>
                                <li>Spa pedicure</li>
                                <li>Nailpolish toenails</li>
                                <li>Gelish toenails</li>
                                <li>Knees</li>
                                <li>Nail reconstruction</li>
                              </ul>
                              <ul className={styles.icon}>
                                <Row className={styles.rowicons}>
                                  <Col>
                                    {/* <FaQuestionCircle className={styles.iconq} /> */}
                                    <Link to="/services"><Button size="lg" className={styles.iconbtn}><FaQuestionCircle className={styles.iconq} /></Button></Link>
                                  </Col>
                                  <Col>
                                    {/* <FaCalendarAlt className={styles.iconq} /> */}
                                    <Link to="/apt"><Button size="lg" className={styles.iconbtn}><FaCalendarAlt className={styles.iconq} /></Button></Link>
                                  </Col>
                                </Row>
                              </ul>
                          </div>
                      </div>
                  </div>
                  </Fade>  
              </Col>
              <Col md={4}>
              <Fade right duration={2000}>
                  <div className={styles.box}>
                    <MassImg />
                      <div className={styles.boxcontent}>
                          <div className={styles.content}>
                              <h3 className={styles.title}>Massage</h3>
                              <ul>
                                <li>Full body</li>
                                <li>Cupping</li>
                                <li>Legs</li>
                                <li>Back</li>
                                <li>Face and Head</li>
                              </ul>
                              <ul className={styles.icon}>
                                <Row className={styles.rowicons}>
                                  <Col>
                                    {/* <FaQuestionCircle className={styles.iconq} /> */}
                                    <Link to="/services"><Button size="lg" className={styles.iconbtn}><FaQuestionCircle className={styles.iconq} /></Button></Link>
                                  </Col>
                                  <Col>
                                    {/* <FaCalendarAlt className={styles.iconq} /> */}
                                    <Link to="/apt"><Button size="lg" className={styles.iconbtn}><FaCalendarAlt className={styles.iconq} /></Button></Link>
                                  </Col>
                                </Row>
                              </ul>
                          </div>
                      </div>
                  </div>
                  </Fade>
                </Col>
            </Row>
          </div>
        </div> 
      </div>
    </div>
  </Layout>
  )
}

export default IndexPage
