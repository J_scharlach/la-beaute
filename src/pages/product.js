import React from "react"
import { Link } from "gatsby"
import {  Row, Col, Container } from "react-bootstrap"
import styles from './product.module.css'
import { FaStar, FaShoppingCart, FaHeart } from 'react-icons/fa';
import Lip from '../components/Image/lip'
import Lip1 from '../components/Image/lip1'
import Lip2 from '../components/Image/lip2'
import Lip3 from '../components/Image/lip3'
import Layout from "../components/layout"
import SEO from "../components/seo"

function product() {
    return (
    <Layout pageInfo={{ pageName: "product" }}>
    <SEO title="product" />    
    <Container className={styles.topconproduct}>
        <Container>
            <div>
                <h1 className={styles.h1title}>RANGE OF PRODUCTS</h1>
            </div>
        </Container>
        <Row>
            <Col xs={6} md={3}>
                <div className={styles.productgrid}>
                    <div className={styles.productimage}>
                        <div className={styles.image}>
                            <div className={styles.imagepic}>
                                <div className={styles.actualimg}>
                                    <Lip />
                                </div>
                            </div>
                        </div>
                        <Link to="#"><div className={styles.productlikeicon}><FaHeart /></div></Link>
                    </div>
                    <div className={styles.productcontent}>
                        <div className={styles.rating}>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                        </div>
                        <h3 className={styles.title}><Link to="#" className={styles.linka}>Real Red</Link></h3>
                        <div className={styles.price}>21.00</div>
                        <Link to="#">
                            <div className={styles.addtocart}>
                                <FaShoppingCart />
                            </div>
                        </Link>
                    </div>
                </div>
            </Col>
            <Col xs={6} md={3}>
            <div className={styles.productgrid}>
                    <div className={styles.productimage}>
                    <div className={styles.image}>
                            <div className={styles.imagepic}>
                                <div className={styles.actualimg}>
                                    <Lip1 />
                                </div>
                            </div>
                            </div>
                        <Link to="#"><FaHeart className={styles.productlikeicon}/></Link>
                    </div>
                    <div className={styles.productcontent}>
                        <div className={styles.rating}>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                        </div>
                        <h3 className={styles.title}><Link to="#" className={styles.linka}>Red</Link></h3>
                        <div className={styles.price}>21.00</div>
                        <Link to="#">
                            <div className={styles.addtocart}>
                                <FaShoppingCart />
                            </div>
                        </Link>
                    </div>
                </div>
            </Col>
            <Col xs={6} md={3}>
                <div className={styles.productgrid}>
                    <div className={styles.productimage}>
                        <div className={styles.image}>
                            <div className={styles.imagepic}>
                                <div className={styles.actualimg}>
                                    <Lip2 />
                                </div>
                            </div>
                        </div>
                        <Link to="#"><FaHeart className={styles.productlikeicon}/></Link>
                    </div>
                    <div className={styles.productcontent}>
                        <div className={styles.rating}>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                        </div>
                        <h3 className={styles.title}><Link to="#" className={styles.linka}>Orange</Link></h3>
                        <div className={styles.price}>25.00</div>
                        <Link to="#">
                            <div className={styles.addtocart}>
                                <FaShoppingCart />
                            </div>
                        </Link>
                    </div>
                </div>
            </Col>
            <Col xs={6} md={3}>
            <div className={styles.productgrid}>
                    <div className={styles.productimage}>
                        <div className={styles.image}>
                            <div className={styles.imagepic}>
                                <div className={styles.actualimg}>
                                    <Lip3 />
                                </div>
                            </div>
                        </div>
                        <Link to="#"><FaHeart className={styles.productlikeicon}/></Link>
                    </div>
                    <div className={styles.productcontent}>
                        <div className={styles.rating}>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                            <div><FaStar className={styles.fas} /></div>
                        </div>
                        <h3 className={styles.title}><Link to="#" className={styles.linka}>Diff Red</Link></h3>
                        <div className={styles.price}>50.00</div>
                        <Link to="#">
                            <div className={styles.addtocart}>
                                <FaShoppingCart />
                            </div>
                        </Link>
                    </div>
                </div>
            </Col>
        </Row>
    </Container> 
    </Layout>  
    )
}

export default product
