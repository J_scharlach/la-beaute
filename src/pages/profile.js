import React, { useEffect, useState }from 'react'
import SEO from "../components/seo"
import Layout from "../components/layout"
import { Link } from "gatsby"
import LogoNav from '../components/Image/logonav'
import styles from './profile.module.css'
import {  Container, Row, Col } from "react-bootstrap"
import firebase from "gatsby-plugin-firebase"
import { firestore } from "../utils/firebase"

const Profile = () => {

    const [person, setPerson] = useState([])

  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
      var ref = firestore.collection("users")

      ref.where('user_id', '==', firebase.auth().currentUser.uid).get()
          .then(snapshot => {
            snapshot.forEach(doc => {
            var client = doc.data()
            setPerson(client)
            })
          })
      }else{
          console.log('nope')
      }
    })
  }, [])

    return (
    <Layout pageInfo={{ pageName: "profile" }}>
    <SEO title="Profile"/>
        <Container className={styles.topconsignin}>
            <div className={styles.conform} id="fromreg">
                <div className={styles.logindiv}>
                    <div  className={styles.logodiv}>
                        <Link to="/">
                            <LogoNav className={styles.logo} />
                        </Link>
                    </div>
                    <div className={styles.title}>Profile</div>
                    <div className={styles.fields}>
                        <Row>
                            <Col sm={6}>
                                <div className={styles.name}>{person.fname}</div>
                            </Col>
                            <Col sm={6}>
                                <div className={styles.name}>{person.lname}</div>
                            </Col>
                        </Row>
                        <div>
                            <div className={styles.name}>{person.email}</div>
                        </div>
                        <div>
                            <div className={styles.name}>{person.address}</div>
                        </div>
                        <div>
                            <div className={styles.name}>{person.phone}</div>
                        </div>
                        <div>
                            <div className={styles.name}>{person.bday}</div>
                        </div>
                        <div className={styles.title}>Previous Sessions</div>
                        <section className={styles.treatdiv}>
                        <div>    
                        <Row className={styles.rowdiv}>
                            <Col xs={12} sm={6} className={styles.datetreat}>
                                <p className={styles.datetreatp1}>Date</p>
                                <p className={styles.datetreatp3}>2021-07-22</p>
                            </Col>
                            <Col xs={12} sm={6} className={styles.datetreat}>
                                <p className={styles.datetreatp1}>Treatment</p>
                                <p className={styles.datetreatp3}>Nails</p>
                            </Col>
                        </Row>
                        </div>
                        <div>
                            <p className={styles.Notesabouttreatment}>Notes</p>
                            <p className={styles.Notesabouttreatment2}>What ever you do to nails.  I have no idea.  Maybe put your color and process you did to them.  That is if you do some different types of things.  I am a guy what do I know.</p>
                        </div>
                        </section>
                    </div>
                </div>
                {/* {user ? <Button>hit me</Button> : null} */}
            </div>
        </Container>
    </Layout>     
    )
}

export default Profile
