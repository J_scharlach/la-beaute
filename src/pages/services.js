import React from 'react'
import Layout from "../components/layout"
import SEO from "../components/seo"
import {  Container } from "react-bootstrap"
import styles from './services.module.css'
import Pl1 from '../components/Image/pl1'
import Pl2 from '../components/Image/pl2'
import { Link } from 'gatsby'

function services() {
    return (
    <Layout pageInfo={{ pageName: "services" }}>
    <SEO title="services" />  
    <Container className={styles.topconservices}>
        <div>
            <h1 className={styles.h1title}>TREATMENTS</h1>
            <div>
                <Pl1 />
                <Pl2 />
            </div>
            <div className={styles.signinbuttondiv} >
                <button className={styles.signinbutton} size="lg"><Link to="/apt" className={styles.linkcolor}>Book an apt</Link></button>
            </div>
        </div>
    </Container>
    </Layout>
    )
}

export default services