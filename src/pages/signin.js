import React, { useState, useContext } from 'react'
import Layout from "../components/layout"
import SEO from "../components/seo"
import {  Container, Row, Col, Form  } from "react-bootstrap"
import styles from './signin.module.css'
import { Link, navigate } from "gatsby"
import LogoNav from '../components/Image/logonav'
import firebase from "gatsby-plugin-firebase"
import {AuthContext} from "../context/auth"


const Signin = () => {

    const [data, setData] = useState({
        email:'',
        password:'',
        error: null
        })
     
        const { setUser } = useContext(AuthContext)
    
     const handleChange = (e) => {
         setData({ ...data, [e.target.name]: e.target.value })
     }


const handleLogin = async e =>{
    e.preventDefault()
    setData({ ...data, error: null })
            try{
            const result = await firebase
                .auth()
                .signInWithEmailAndPassword(data.email, data.password)
            setUser(result)
                navigate('/')
            } catch (err) {
                setData({...data, error: err.message})
            }
        }

    return (
    <Layout pageInfo={{ pageName: "signin" }}>
    <SEO title="login" />  
        <Container className={styles.topconsignin}>
            <Row>
                <Col xs={11} md={12}>
                <Form onSubmit={handleLogin} id="fromlogin">
                    <div className={styles.logindiv}>
                        <div  className={styles.logodiv}>
                            <Link to="/">
                                <LogoNav className={styles.logo} />
                            </Link>
                        </div>
                        <div className={styles.fields}>
                            {/* <div className={styles.username}><input type="email" className={styles.userinput} placeholder="email address" onChange={event => setEmail(event.target.value)}/></div> */}
                            <div className={styles.username}><input type="email" className={styles.userinput} placeholder="email address" name="email" value={data.email} onChange={handleChange}/></div>
                            <div className={styles.password}><input type="password" className={styles.passinput} placeholder="password" name="password" value={data.password} onChange={handleChange}/></div>
                        </div>
                        <button className={styles.signinbutton} type="submit">Login</button>
                        <div className={styles.link}>
                            {/* <Link to="#">Forgot password?</Link> or  */}
                            <Link to="/signup">Register</Link>
                        </div> 
                    </div>
                </Form>
                </Col>
                {data.error ? <p style={{ color: "red" }}>{data.error}</p> : null} 
            </Row> 
        </Container>
    </Layout>
    )
}

export default Signin