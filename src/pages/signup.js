import React, { useState } from 'react'
import Layout from "../components/layout"
import SEO from "../components/seo"
import {  Container, Form } from "react-bootstrap"
import styles from './signup.module.css'
import { Link, navigate } from "gatsby"
import LogoNav from '../components/Image/logonav'
import { firestore } from "../utils/firebase"
import "firebase/firestore"
import 'firebase/auth'
import slugify from 'slugify'
import firebase from "gatsby-plugin-firebase"
// import { useHistory } from "react-router-dom";

const Signup = () => {

    const [email, setEmail] = useState('');
    const [fname, setFname] = useState('');
    const [lname, setLname] = useState('');
    const [address, setAddress] = useState('');
    const [phone, setPhone] = useState('');
    const [bday, setBday] = useState('');
    const [password, setPassword] = useState('');
    // const history = useHistory()

const handleClear = (e) =>{
    e.preventDefault();
    document.getElementById("fromreg").reset();
    }

    const handleSubmituser = (e) => {
        e.preventDefault();
            if(email && password && fname && lname && address && phone && bday){
            var slug
              slug = slugify(fname + lname, {replacement: '-', remove: /[$*_+~.()'"!\-:@]/g, lower: true})
              firestore.collection('users').doc(slug).get()
              .then(doc => {
                if(doc.exists){
                 alert('this name is already in the database.  Please add the first initial of your middle name after your first name below')
                  
                } else {
                // this alias does not yet exists in the db
                firebase.auth().createUserWithEmailAndPassword(email, password)
                  .then(cred => {
                    firestore.collection('users').doc(slug).set({
                      user_id: cred.user.uid,
                      fname: fname,
                      lname: lname,
                      email: email,
                      address: address,
                      phone: phone,
                      slug: slug,
                      adminJ: "joel",
                      adminM: "marine"
                    })
                  }).then(() => {
                    navigate('/')
                  })
                  .catch(err => {
                    console.log(err.message)
                  }) 
                }
              })
            } else {
                alert('Please fill in all fields')
          }
        }
    return (
    <Layout pageInfo={{ pageName: "register" }}>
    <SEO title="register" />  
        <Container className={styles.topconsignin}>
        <Form className={styles.conform} onSubmit={handleSubmituser} id="fromreg">
            <div className={styles.logindiv}>
                <div  className={styles.logodiv}>
                    <Link to="/">
                        <LogoNav className={styles.logo} />
                    </Link>
                </div>
                <div className={styles.title}>REGISTER</div>
                <div className={styles.fields}>
                    <div>
                        <div className={styles.username}><input type="text" className={styles.userinput} placeholder="first name" onChange={event => setFname(event.target.value)}/></div>
                    </div>
                    <div>
                        <div className={styles.username}><input type="text" className={styles.userinput} placeholder="last name" onChange={event => setLname(event.target.value)}/></div>
                    </div>
                    <div>
                        <div className={styles.password}><input type="email" className={styles.passinput} placeholder="email" onChange={event => setEmail(event.target.value)}/></div>
                    </div>
                    <div>
                        <div className={styles.username}><input type="password" className={styles.userinput} placeholder="create password"onChange={event => setPassword(event.target.value)} /></div>
                    </div>
                    <div>
                        <div className={styles.password}><input type="text" className={styles.passinput} placeholder="address" onChange={event => setAddress(event.target.value)}/></div>
                    </div>
                    <div>
                        <div className={styles.username}><input type="text" className={styles.userinput} placeholder="phone number" id="phone" name="phone" onChange={event => setPhone(event.target.value)} /></div>
                    </div>
                    <div className={styles.bdaydiv}>
                        <div className={styles.bday}><input type="date" className={styles.passinput} placeholder="birthday" onChange={event => setBday(event.target.value)}/></div>
                    </div>
                    {/* <div class="first-example">
                        <input type="password" id="password-field" />
                        <i id="pass-status" class="fa fa-eye" aria-hidden="true" onClick="viewPassword()"></i>
                    </div> */}
                </div>
                <div>
                    <button className={styles.signinbutton} type="submit">Register</button>
                </div>
                <div className={styles.bottombtn}>
                    <button className={styles.signinbutton1} onClick={handleClear}>Clear fields</button>
                </div>
            </div>
        </Form>    
        </Container>
    </Layout>
    )
}

export default Signup

