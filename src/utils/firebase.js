import firebase from "firebase/app"
import "firebase/firestore"

const config = {
    apiKey: "AIzaSyCweOmkZTtJJyAhCPbqYMC3reKKb4V89PA",
    authDomain: "labeauteinfinie-6507d.firebaseapp.com",
    projectId: "labeauteinfinie-6507d",
    storageBucket: "labeauteinfinie-6507d.appspot.com",
    messagingSenderId: "1016851416008",
    appId: "1:1016851416008:web:3923a7715d999e9c8cf6fd",
    measurementId: "G-1HPT92XVRB"
}

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

const firestore = firebase.firestore()

export { firestore }